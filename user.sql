-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Sam 15 Décembre 2018 à 15:40
-- Version du serveur :  10.1.24-MariaDB-6
-- Version de PHP :  7.0.22-3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `user`
--

-- --------------------------------------------------------

--
-- Structure de la table `Ajouter`
--

CREATE TABLE `Ajouter` (
  `nomobjet` varchar(30) NOT NULL,
  `pseudo` varchar(30) NOT NULL,
  `favoris` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `Ajouter`
--

INSERT INTO `Ajouter` (`nomobjet`, `pseudo`, `favoris`) VALUES
('Iphone X', 'jimmy', 1),
('Macbook pro', 'jimmy', 1),
('Manteau', 'jimmy', 0),
('Manteau en laine', 'jimmy', 1);

-- --------------------------------------------------------

--
-- Structure de la table `Description`
--

CREATE TABLE `Description` (
  `nomobjet` varchar(30) NOT NULL,
  `cible` varchar(30) DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  `agemin` int(2) DEFAULT NULL,
  `agemax` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `Description`
--

INSERT INTO `Description` (`nomobjet`, `cible`, `type`, `agemin`, `agemax`) VALUES
('Iphone X', 'neutre', 'electronique', 12, 40),
('Lampe Minnie', 'feminin', 'jouet', 5, 9),
('Macbook pro', 'neutre', 'electronique', 8, 50),
('Manteau', 'masculin', 'vetement', 20, 35),
('Manteau en laine', 'feminin', 'vetement', 20, 35),
('Nintendo 3DS', 'neutre', 'jeuvideo', 7, 35),
('Nintendo Switch', 'neutre', 'jeuvideo', 3, 35),
('Playstation 4', 'neutre', 'jeuvideo', 10, 35),
('Pokemon LG', 'neutre', 'jeuvideo', 3, 35),
('Pull', 'masculin', 'vetement', 20, 35),
('Pull en Maille', 'feminin', 'vetement', 20, 35),
('Quad Electrique', 'masculin', 'jouet', 3, 7),
('Red Dead Redemption 2', 'masculin', 'jeuvideo', 18, 35),
('Robot Cosmo', 'neutre', 'jouet', 8, 12),
('Salle de classe', 'neutre', 'jouet', 3, 7),
('Spider-man ', 'masculin', 'jeuvideo', 12, 35),
('SSB', 'neutre', 'jeuvideo', 12, 35),
('TV Led', 'neutre', 'electronique', 10, 60),
('Zelda BOTW', 'neutre', 'jeuvideo', 12, 35);

-- --------------------------------------------------------

--
-- Structure de la table `Magasin`
--

CREATE TABLE `Magasin` (
  `nom` varchar(30) NOT NULL,
  `site` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `Magasin`
--

INSERT INTO `Magasin` (`nom`, `site`) VALUES
('Cultura', 'https://www.cultura.com/'),
('Fnac', 'https://www.fnac.com/'),
('H&M', 'https://www2.hm.com/fr_fr/'),
('Micromania', 'https://www.micromania.fr/'),
('Toys R us', 'http://www.toysrus.fr');

-- --------------------------------------------------------

--
-- Structure de la table `Objet`
--

CREATE TABLE `Objet` (
  `nom` varchar(30) NOT NULL,
  `marque` varchar(30) DEFAULT NULL,
  `prix` int(4) DEFAULT NULL,
  `image` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `Objet`
--

INSERT INTO `Objet` (`nom`, `marque`, `prix`, `image`) VALUES
('Iphone X', 'Apple', 950, 'IPHONE'),
('Lampe Minnie', 'Worlds Appart', 19, 'LAMP'),
('Macbook pro', 'Apple', 560, 'MAC'),
('Manteau', 'H&M', 99, 'MANTEAU'),
('Manteau en laine', 'H&M', 99, 'LAINE'),
('Nintendo 3DS', 'NINTENDO', 350, '3DS'),
('Nintendo Switch', 'NINTENDO', 299, 'SWITCH'),
('Playstation 4', 'Sonny', 300, 'PS4'),
('Pokemon LG', 'NINTENDO', 63, 'POKEMON'),
('Pull', 'H&M', 12, 'PULL'),
('Pull en Maille', 'H&M', 18, 'MAILLE'),
('Quad Electrique', 'injusa', 35, 'QUAD'),
('Red Dead Redemption 2', 'Sonny', 68, 'REDDEAD'),
('Robot Cosmo', 'Anki', 149, 'COSMO'),
('Salle de classe', 'PLAYMOBIL', 20, 'PLAYMOBIL'),
('Spider-man ', 'Sonny', 65, 'SPIDERMAN'),
('SSB', 'NINTENDO', 70, 'SSB'),
('TV Led', 'Samsung', 750, 'TV'),
('Zelda BOTW', 'NINTENDO', 55, 'ZELDA');

-- --------------------------------------------------------

--
-- Structure de la table `Utilisateur`
--

CREATE TABLE `Utilisateur` (
  `pseudo` varchar(30) NOT NULL,
  `motdepasse` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `Utilisateur`
--

INSERT INTO `Utilisateur` (`pseudo`, `motdepasse`) VALUES
('jimmy', 'soussan');

-- --------------------------------------------------------

--
-- Structure de la table `Vendre`
--

CREATE TABLE `Vendre` (
  `nomobjet` varchar(30) NOT NULL,
  `nommag` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `Vendre`
--

INSERT INTO `Vendre` (`nomobjet`, `nommag`) VALUES
('Iphone X', 'Cultura'),
('Iphone X', 'Fnac'),
('Lampe Minnie', 'Toys R us'),
('Macbook pro', 'Cultura'),
('Macbook pro', 'Fnac'),
('Manteau', 'H&M'),
('Manteau en laine', 'H&M'),
('Nintendo 3DS', 'Cultura'),
('Nintendo 3DS', 'Fnac'),
('Nintendo 3DS', 'Micromania'),
('Nintendo 3DS', 'Toys R us'),
('Nintendo Switch', 'Cultura'),
('Nintendo Switch', 'Fnac'),
('Nintendo Switch', 'Micromania'),
('Nintendo Switch', 'Toys R us'),
('Playstation 4', 'Cultura'),
('Playstation 4', 'Fnac'),
('Playstation 4', 'Micromania'),
('Pokemon LG', 'Cultura'),
('Pokemon LG', 'Fnac'),
('Pokemon LG', 'Micromania'),
('Pokemon LG', 'Toys R us'),
('Pull', 'H&M'),
('Pull en Maille', 'H&M'),
('Quad Electrique', 'Toys R us'),
('Red Dead Redemption 2', 'Cultura'),
('Red Dead Redemption 2', 'Fnac'),
('Red Dead Redemption 2', 'Micromania'),
('Robot Cosmo', 'Cultura'),
('Robot Cosmo', 'Fnac'),
('Robot Cosmo', 'Toys R us'),
('Salle de classe', 'Toys R us'),
('Spider-man ', 'Cultura'),
('Spider-man ', 'Fnac'),
('Spider-man ', 'Micromania'),
('SSB', 'Cultura'),
('SSB', 'Fnac'),
('SSB', 'Micromania'),
('SSB', 'Toys R us'),
('TV Led', 'Fnac'),
('Zelda BOTW', 'Cultura'),
('Zelda BOTW', 'Fnac'),
('Zelda BOTW', 'Micromania'),
('Zelda BOTW', 'Toys R us');

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `V_Favoris`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `V_Favoris` (
`nom` varchar(30)
,`marque` varchar(30)
,`prix` int(4)
,`image` varchar(50)
,`nomobjet` varchar(30)
,`pseudo` varchar(30)
,`favoris` tinyint(1)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `V_Magasin`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `V_Magasin` (
`nom` varchar(30)
,`site` varchar(30)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `V_Objet`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `V_Objet` (
`nom` varchar(30)
,`marque` varchar(30)
,`prix` int(4)
,`image` varchar(50)
);

-- --------------------------------------------------------

--
-- Structure de la vue `V_Favoris`
--
DROP TABLE IF EXISTS `V_Favoris`;

CREATE ALGORITHM=UNDEFINED DEFINER=`user`@`localhost` SQL SECURITY DEFINER VIEW `V_Favoris`  AS  select `V_Objet`.`nom` AS `nom`,`V_Objet`.`marque` AS `marque`,`V_Objet`.`prix` AS `prix`,`V_Objet`.`image` AS `image`,`Ajouter`.`nomobjet` AS `nomobjet`,`Ajouter`.`pseudo` AS `pseudo`,`Ajouter`.`favoris` AS `favoris` from (`V_Objet` join `Ajouter`) where ((`Ajouter`.`nomobjet` = `V_Objet`.`nom`) and (`Ajouter`.`favoris` <> 0)) ;

-- --------------------------------------------------------

--
-- Structure de la vue `V_Magasin`
--
DROP TABLE IF EXISTS `V_Magasin`;

CREATE ALGORITHM=UNDEFINED DEFINER=`user`@`localhost` SQL SECURITY DEFINER VIEW `V_Magasin`  AS  select `Magasin`.`nom` AS `nom`,`Magasin`.`site` AS `site` from `Magasin` ;

-- --------------------------------------------------------

--
-- Structure de la vue `V_Objet`
--
DROP TABLE IF EXISTS `V_Objet`;

CREATE ALGORITHM=UNDEFINED DEFINER=`user`@`localhost` SQL SECURITY DEFINER VIEW `V_Objet`  AS  select `Objet`.`nom` AS `nom`,`Objet`.`marque` AS `marque`,`Objet`.`prix` AS `prix`,`Objet`.`image` AS `image` from `Objet` ;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Ajouter`
--
ALTER TABLE `Ajouter`
  ADD PRIMARY KEY (`nomobjet`,`pseudo`),
  ADD KEY `pseudo` (`pseudo`);

--
-- Index pour la table `Description`
--
ALTER TABLE `Description`
  ADD PRIMARY KEY (`nomobjet`);

--
-- Index pour la table `Magasin`
--
ALTER TABLE `Magasin`
  ADD PRIMARY KEY (`nom`);

--
-- Index pour la table `Objet`
--
ALTER TABLE `Objet`
  ADD PRIMARY KEY (`nom`);

--
-- Index pour la table `Utilisateur`
--
ALTER TABLE `Utilisateur`
  ADD PRIMARY KEY (`pseudo`);

--
-- Index pour la table `Vendre`
--
ALTER TABLE `Vendre`
  ADD PRIMARY KEY (`nomobjet`,`nommag`),
  ADD KEY `nommag` (`nommag`);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Ajouter`
--
ALTER TABLE `Ajouter`
  ADD CONSTRAINT `Ajouter_ibfk_1` FOREIGN KEY (`nomobjet`) REFERENCES `Objet` (`nom`),
  ADD CONSTRAINT `Ajouter_ibfk_2` FOREIGN KEY (`pseudo`) REFERENCES `Utilisateur` (`pseudo`);

--
-- Contraintes pour la table `Description`
--
ALTER TABLE `Description`
  ADD CONSTRAINT `Description_ibfk_1` FOREIGN KEY (`nomobjet`) REFERENCES `Objet` (`nom`);

--
-- Contraintes pour la table `Vendre`
--
ALTER TABLE `Vendre`
  ADD CONSTRAINT `Vendre_ibfk_1` FOREIGN KEY (`nomobjet`) REFERENCES `Objet` (`nom`),
  ADD CONSTRAINT `Vendre_ibfk_2` FOREIGN KEY (`nommag`) REFERENCES `Magasin` (`nom`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
